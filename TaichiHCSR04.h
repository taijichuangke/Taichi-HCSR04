/*
TaichiHC库
by 太极创客 Taichi-Maker / www.taichi-maker.com
本Arduino库旨在读取HCSR04超声传感器模块所测量到的距离数值。
This Arduino Lib is designed for the HCSR04 ultra sound distance sensor.
*/
#ifndef TAICHIHCSR04_H_
#define TAICHIHCSR04_H_
#include <Arduino.h>

class TaichiHC{
  public:
    TaichiHC();
    TaichiHC(int trigPin, int echoPin);
    int getDistCm();
    int getDistInch();
  private:
    int trig;
    int echo;
    long duration, cm, inches;
};

#endif
