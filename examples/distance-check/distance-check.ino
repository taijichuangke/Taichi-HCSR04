/*
Arduino 驱动HC-SR04 超声波测距传感器模块
by 太极创客 Taichi-Maker
http://www.taichi-maker.com
2019-06-15

本程序旨在演示如何使用Taichi-HCSR04库读取HC-SR04超声波测距传感器模块读数。
程序运行后，传感器将感应到的距离信息并通过Arduino IDE的串口监视器显示。

This code demostrate how to use Taichi-HCSR04 Ultra Sound Distance Sensor to measure
distance. The measurement results will be displayed in Arduino IDE's Serial Monitor.

接线方法/Circuit Connection

HC-SR04   -------   Arduino
  VCC     -------    +5VDC
  Trig    -------     7
  Echo    -------     8
  GND     -------     GND

如需获得HC-SR04超声波测距传感器模块和Arduino的更多信息
请参阅太极创客网站：http://www.taichi-maker.com

For more information about HC-SR04 Ultra Sound Distance Sensor
please refer to http://www.taichi-maker.com
*/

#include <TaichiHCSR04.h>

TaichiHC hc(7, 8); // 建立对象 hcsr04(int trigPin, int echoPin);
// TaichiHC hc(); // 建立对象(无参数情况trig = 7, echo = 8) / If no paramenter provided, default trig = 7, echo = 8.

void setup() {
  Serial.begin (9600);
}

void loop(){
  int distCM = hc.getDistCm();  //读取传感器测量举例
  Serial.print(distCM);         //通过串口监视器输出测量距离值
  Serial.println("cm");         //单位是厘米

  int distInch = hc.getDistInch(); //读取传感器测量举例
  Serial.print(distInch);          //通过串口监视器输出测量距离值
  Serial.println("inch");          //单位是英寸

  delay(1000);
}
