#include <Arduino.h>
#include "TaichiHCSR04.h"

TaichiHC::TaichiHC(){
  trig = 7;
  echo = 8;

  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
}

TaichiHC::TaichiHC(int trigPin, int echoPin){
  trig = trigPin;
  echo = echoPin;

  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
}

int TaichiHC::getDistCm(){
  // 传感器由10微秒来触发读数。在发送读数脉冲前先发送一个5微秒的低电平确保读数稳定。
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //读取echo引脚的脉冲信号。该信号为传感器测量读数信号。
  // Read the signal from the sensor。
  duration = pulseIn(echo, HIGH);

  // 将读取到的脉冲信号转换为测量距离（单位为厘米）
  // convert the time into a distance
  cm = (duration/2) / 29.1;
  return cm;
}

int TaichiHC::getDistInch(){
  // 传感器由10微秒来触发读数。在发送读数脉冲前先发送一个5微秒的低电平确保读数稳定。
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  //读取echo引脚的脉冲信号。该信号为传感器测量读数信号。
  // Read the signal from the sensor。
  duration = pulseIn(echo, HIGH);

  // 将读取到的脉冲信号转换为测量距离（单位为英寸）
  // convert the time into a distance
  inches = (duration/2) / 74;
  return inches;
}
